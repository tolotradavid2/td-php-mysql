<?php

class Model
{
    static $connections = array();

    public $tables = false;

    protected $conf = 'default';

    public $db;

    protected $primaryKey = 'id';

    public function __construct()
    {
        if ($this->tables === false) {
            $this->tables = strtolower(get_class($this)) . 's';
        }
        $conf  = Conf::$databases[$this->conf];
        if (isset(Model::$connections[$this->conf])) {
            $this->db = Model::$connections[$this->conf];
            return true;
        }

        try {

            $pdo  = new PDO(
                'mysql:host=' . $conf['host'] . ';dbname=' . $conf['database'] . ';',
                $conf['login'],
                $conf['password'],
                array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                )
            );
            Model::$connections[$this->conf] = $pdo;
            $this->db = $pdo;
        } catch (PDOException $e) {
            if (Conf::$debug >= 1) {
                die($e->getMessage());
            } else {
                die('Impossible de se connecter à la base de données');
            }
        }
    }

    /**
     * find one element in the database
     * @param {array} $req ['conditions' => ...]
     */
    public function find(array $req)
    {
        $sql = "SELECT ";

        // test

        if (isset($req['fields'])) {
            if (is_array($req['fields'])) {
                $sql .= implode(', ', $req['fields']);
            } else {
                $sql .= $req['fields'];
            }
        } else {
            $sql .= "*";
        }

        $sql .= " FROM {$this->tables} AS " . get_class($this);

        if (isset($req['conditions'])) {
            $sql .= ' WHERE ';
            if (!is_array($req['conditions'])) {
                $sql .=  $req['conditions'];
            } else {
                $cond = array();
                foreach ($req['conditions'] as $key => $value) {
                    if (!is_numeric($value)) {
                        $value = '"' . htmlspecialchars($value) . '"';
                    }
                    $cond[] = "$key = $value";
                }
                $sql .= implode(' AND ', $cond);
            }
        }

        if (isset($req['limit'])) {
            $sql .= ' LIMIT ' . $req['limit'];
        }
        $pre = $this->db->prepare($sql);
        $pre->execute();
        return $pre->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * find the first element in the database
     * @param {array} $req ['conditions' => ...]
     */
    public function findFirst($sql)
    {
        return current($this->find($sql));
    }

    /**
     * Count articles numbers
     */
    public function findCount($conditions)
    {
        $res = $this->findFirst(['fields' => "COUNT({$this->primaryKey}) AS count", 'conditions' => $conditions]);
        return $res->count;
    }
}
