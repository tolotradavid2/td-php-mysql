<?php

class Controller {

    private $vars = [];

    protected $request = null;

    protected $layout = 'default';

    private $rendered = false;



    public function __construct($request = null){
        if(isset($request)){
            $this->request = $request;
        }
        $this->loadModel('Post');
    }

    /**
     * Call the pages in view
     * @param {string} $view
     * @return bool
     */
    public function render($view){
        if($this->rendered) return false;
        extract($this->vars);
        if(strpos($view, '/') === 0){
            $view = ROOT . DS . 'view' . $view;
        }else{
            $view = ROOT . DS . 'view' . DS . $this->request->controller . DS . $view;
        }
        ob_start();
        require_once $view  . '.html.php';
        $content_for_layout = ob_get_clean();
        require ROOT . DS . 'view' . DS . 'layout' . DS . $this->layout . '.html.php';
        $this->rendered = true;
    }

    /**
     * set in $this->vars the inforamtions
     * @param {string} $key, variable name
     * @param {string} $values, variable values
     */
    public function setVars($key, $values = null){
        if(is_array($key)){
            $this->vars += $key;
        }else{
            $this->vars[$key] = $values;
        }
    }

    /**
     * load one Model
     * @param {string} $name
     */
    public function loadModel($name){
        require_once ROOT . DS . 'model' . DS . $name . '.php';
        if(!isset($this->name)){ 
            $this->$name = new $name(); 
        }
    }

    /**
     * gère les pages introuvables
     * @param {string} $message
     */
    public function e404($message){
        header("HTTP/1.0 404 Not Found");
        $this->setVars('message', $message);
        $this->render('/errors/404'); 
        die();
    }

    /**
     * Permet d'appeler un controller depuis une vue
     * @param {String} $controller
     * @param {string} $action
     * @return mixed
     */
    public function request($controller, $action){
        $controller .= 'Controller';
        require_once ROOT . DS . 'controller' . DS . $controller . '.php';
        $controller  = new $controller();
        return $controller->$action();
    }

    public static function debug($bug){
        $debug = debug_backtrace();
        echo "<p>&nbsp;</p>
            <p>
                <a href='#' 
                onmouseover='return this.parentNode.nextElementSibling.style.display=\"block\";' 
                onmouseout='return this.parentNode.nextElementSibling.style.display=\"none\";'>
                <strong>" . $debug[0]['file'] . "</strong> lignes : " . $debug[0]['line'] . " </a>
            </p>";

        echo '<ol style="display:none;">';
            foreach($debug as $k => $v){
                if($k > 0){
                    echo "<li> <strong>" . $v['file'] . " lignes : " . $v['line'] . "</strong></li>";
                }
            }
        echo '</ol>';

        echo "<pre>";
            print_r($bug);
        echo "</pre>";
    }
}