<?php

class Router
{
    static $routes = [];

    /**
     * permet de parser une URL
     * @param 1 {string} $url
     * @param 2 {object} $url
     * @return bool
     */
    public static function parse($url, $request)
    {
        $url = explode('/', trim($url, '/'));
        $request->controller = $url[0];
        $request->action = isset($url[1]) ? $url[1] : 'index';
        $request->params = array_slice($url, 2);
        return true;
    }
}
