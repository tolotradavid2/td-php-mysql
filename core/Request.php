<?php

class Request {

    public $url;
    public $page = 1;

    public function __construct()
    {
        $this->url = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : 'posts';
        if(isset($_GET['page'])){
            $page = (int) $_GET['page'];
            if(is_int($page)){
                if($page > 0){
                    $this->page = round($page);
                }
            }
        }
    }
}