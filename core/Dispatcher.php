<?php

class Dispatcher{

    protected $request;

    public function __construct()
    {
        $this->request = new Request();

        Router::parse($this->request->url, $this->request);

        $controller = $this->loadController();

        /**
         * @param {function} $controller, c'est le nom de la classe à appeler
         * @param {array} $this->request->action, c'est le nom de la méthode à appeler dans la classe
         * @param {string} $this->request->params, c'est de la page à appeler depuis la méthode
         */
        if(!in_array($this->request->action, 
            array_diff(get_class_methods($controller), 
            get_class_methods('Controller')))
            ){
            $this->error('La page : ' . $this->request->action .
                          ' est introuvable !');
        }

        call_user_func_array(
        [$controller, $this->request->action],
        $this->request->params);

        $controller->render($this->request->action);
    }


    /**
     * permet de charger un controlleur
     * @return {Object}
     */
    public function loadController(){
        $name = ucfirst($this->request->controller).'Controller';
        $file = ROOT . DS . 'controller' . DS . $name . '.php';
        if(!file_exists($file)) $this->error("Désolé la page {$this->request->controller} est introuvable.");
        require $file;
        return new $name($this->request);
    }

    /**
     * treat the errors 
     * @param {string} $message
     */
    public function error($message){
        $controller = new Controller($this->request);
        $controller->e404($message);
    }
}
