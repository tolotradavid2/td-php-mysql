<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= isset($title_for_layout) ? $title_for_layout :  'Mon site' ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://localhost/bootstrap/css/bootstrap.css">
</head>

<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <div class="container">
            <ul class="navbar-nav">
                <li>
                    <a class="nav-link active" href="<?= BASE_URL . '/posts' ?>">Actualités</a>
                </li>
                <?php $pagesMenu = $this->request('Pages', 'getMenu'); ?>
                <?php foreach ($pagesMenu as $key => $value) : ?>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= BASE_URL ?>/pages/view/<?= $value->id ?>"><?= $value->name ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </nav>
    <div class="container">
        <?= $content_for_layout ?>
    </div>
</body>
</html>

