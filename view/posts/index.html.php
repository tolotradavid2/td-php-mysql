<div class="page-header">
    <h1>Mon premier blog</h1>
</div>
<div class="page-content">
    <?php if (!empty($posts)) {
        foreach ($posts as $key => $value) : ?>
            <h3><?= $value->name ?></h3>
            <p><?= $value->content ?></p>
            <p><a href="<?= BASE_URL?>/posts/view/<?= $value->id?>">Voir la suite &rarr;</a></p>
        <?php endforeach;
    } ?>
    <ul class="pagination">
        <li <?= $this->request->page <= 1 ? 'class="page-item disabled"' : 'class="page-item"' ?> >
            <a class="page-link" href="?page=<?= $previousPage ?>">Previous</a>
        </li>
        <?php if (!empty($pageNumber)) {
            for ($i = 1; $i <= $pageNumber; $i++) : ?>
                <li <?= $i == $this->request->page ? 'class="page-item active"' : 'class="active"'?> >
                    <a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a></li>
            <?php endfor;
        } ?>
        <li <?= $this->request->page >= $totalPage ? 'class="page-item disabled"' : 'class="page-item"' ?> >
            <a class="page-link" href="?page=<?= $nextPage ?>">Next</a>
        </li>
    </ul>
</div>