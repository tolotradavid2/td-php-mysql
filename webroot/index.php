<?php

$begin = microtime(true);


define('WEBROOT', dirname(__FILE__));
define('ROOT', dirname(WEBROOT));
define('DS', DIRECTORY_SEPARATOR);
define('CORE', ROOT . DS . 'core');
define('BASE_URL', dirname(dirname($_SERVER['SCRIPT_NAME'])));
define('HOST', $_SERVER['HTTP_HOST']);

include CORE . DS . 'includes.php';

new Dispatcher;

?>
<div style="position:fixed; bottom: 0; right:2; background-color:red; width:100%; padding:8px; text-align:right;color:white;">
    <?php echo 'Page générée en : ' . round(microtime(true) - $begin, 5) . ' secondes'; ?>
</div>

