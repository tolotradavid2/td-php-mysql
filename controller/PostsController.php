<?php

class PostsController extends Controller
{


    /**
     * Permet de lister les articles
     */
    public function index()
    {
        $perPage = 1;

        $conditions =
            [
                'type' => 'post',
                'online' => 1
            ];
        $d['posts'] = $this->Post->find(
            [
                'conditions' => $conditions,
                'limit' => ($perPage * ($this->request->page - 1)) . ', ' . $perPage
            ]
        );
        $d['totalPage'] = $this->Post->findCount($conditions);
        $d['previousPage'] = $this->request->page > 1 ? $this->request->page - 1 : $this->request->page;
        $d['pageNumber'] = ceil($d['totalPage'] / $perPage);
        $d['nextPage'] = $this->request->page + 1;

        if($this->request->page > $d['totalPage']){
            $this->e404('Désolé, la page que vous avez demandé est introuvable');
        }
        $this->setVars($d);
    }

    /**
     * Permet d'afficher une page
     * @param $id
     */
    public function view($id)
    {
        $conditions =
            [
                'id' => $id,
                'type' => 'post',
                'online' => 1
            ];
            $d['page'] = $this->Post->findFirst(["conditions" => $conditions]);
            if (empty($d['page'])) {
            $this->e404("L'article {$id} est introuvable.");
        }
        $this->setVars($d);
    }
}
