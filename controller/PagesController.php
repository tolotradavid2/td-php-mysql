<?php

class PagesController extends Controller
{

    public function view($id = null)
    {
        $conditions = 
            [
                'id' => $id,
                'type' => 'page',
                'online' => 1
            ];
        $d['page'] = $this->Post->findFirst(array(
            'conditions' => $conditions
        ));
        if (empty($d['page'])) {
            $this->e404('La page ' . $id . ' n\'a pas été trouvée !');
        }

        $this->setVars($d);
    }


    /**
     * Permet d'obtenir le menu en haut
     */
    public function getMenu()
    {
        return $this->Post->find(array(
            'conditions' => array('online' => 1, 'type' => 'page')
        ));
    }
}
